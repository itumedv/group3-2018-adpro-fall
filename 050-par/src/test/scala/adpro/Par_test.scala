package adpro

import org.scalatest.FreeSpec
import java.util.concurrent.{ExecutorService, Executors, TimeUnit}

import adpro.Par.Par

class CHLEtests extends FreeSpec {

  lazy val fail = throw new RuntimeException("Result was evaluated");

  val parFail = Par.lazyUnit[Int] (fail)

  val exe = Executors.newFixedThreadPool(10)

  val single = Executors.newSingleThreadExecutor()

  def run[A] (pa:Par[A]) : A =
    Par.run (exe) (pa).get()

  def runSingle[A] (pa:Par[A]) : A =
    Par.run (single) (pa).get(2000, TimeUnit.MILLISECONDS)

  "Exercise01" - {
    "Function is not evaluated" in {
      Par.asyncF[Int, Unit] (i => fail)
    }

    "Calling function gives correct result" in {
      val async = Par.asyncF[Int, Int] (i => 2 * i)
      assert(42 == Par.run (exe) (async (21)).get())
    }

    "Calling function requires only one thread" in {
      val async = Par.asyncF[Int, Int] (i => 2 * i)
      assert(42 == Par.run (single) (async (21)).get())
    }
  }

  "Exercise02" - {
    val list = Par.unit[Int] (42)::Par.unit[Int] (43)::Nil
    val seq = Par.sequence(list)
    "sequence gives correct list" in {
      assert(run (seq) == 42::43::Nil)
    }

    "sequence requires only one thread" in {
      assert(runSingle (seq) == 42::43::Nil)
    }
  }

  "Exercise03" - {
    val list = 41::42::43::Nil
    lazy val filter = Par.parFilter (list) (i => i != 42)
    "parFilter gives correct result" in {
      assert(run (filter) == 41::43::Nil)
    }

    "parFilter requires only one thread" in {
      assert(runSingle (filter) == 41::43::Nil)
    }
  }

  "Exercise04" - {
    val par1 = Par.unit (10)
    val par2 = Par.unit (3)
    val par3 = Par.unit (12)
    val mapping = Par.map3 (par1, par2, par3) ((i1, i2, i3) => i1 * i2 + i3)
    "map3 returns correct result" in {
      assert(run(mapping) == 42)
    }
  }

  "Exercise05" - {
    "choice does not evaluate false case when true" in {
      assert(run(Par.choice (Par.unit (true)) (Par.unit(42), parFail)) == 42)
    }

    "choice does not evaluate true case when false" in {
      assert(run(Par.choice (Par.unit (false)) (parFail, Par.unit(42))) == 42)
    }

    "choiceN uses correct index and only evaluates one result" in {
      val list = parFail::Par.unit (42)::parFail::Nil
      assert(run(Par.choiceN (Par.unit (1)) (list)) == 42)
    }
  }

  "Exercise06" - {
    "choiceViaChooser does not evaluate false case when true" in {
      assert(run(Par.choiceViaChooser (Par.unit (true)) (Par.unit(42), parFail)) == 42)
    }

    "choiceViaChooser does not evaluate true case when false" in {
      assert(run(Par.choiceViaChooser (Par.unit (false)) (parFail, Par.unit(42))) == 42)
    }

    "choiceNviaChooser uses correct index and only evaluates one result" in {
      val list = parFail::Par.unit (42)::parFail::Nil
      assert(run(Par.choiceNviaChooser (Par.unit (1)) (list)) == 42)
    }
  }

  "Exercise07" - {
    "join returns corresponding Par" in {
      val par = Par.lazyUnit (42)
      assert(run(Par.join (Par.unit(par))) == run(par))
    }
  }
}
