// Advanced Programming. Andrzej Wasowski. IT University
// To execute this example, run "sbt run" or "sbt test" in the root dir of the project
// Spark needs not to be installed (sbt takes care of it)

import org.apache.spark.ml.feature.Tokenizer
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.ml.linalg.Vectors
import scala.collection._
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}

object Main {

	type Embedding       	= (String, List[Double])
	type ParsedReview    	= (Integer, String, Double)
	type FormattedData	= (Int, String, Double, scala.List[String])
	type WordData		= (Int, String, Double, String)
	type PreVector		= (Int, scala.List[scala.List[Double]])
	type IdVectorList    	= (Int, org.apache.spark.ml.linalg.Vector)
	type VectorOverall	= (org.apache.spark.ml.linalg.Vector, Double)

	org.apache.log4j.Logger getLogger "org"  setLevel (org.apache.log4j.Level.WARN)
	org.apache.log4j.Logger getLogger "akka" setLevel (org.apache.log4j.Level.WARN)
	val spark =  SparkSession.builder
		.appName ("Sentiment")
		.master  ("local[9]")
		.getOrCreate

  	import spark.implicits._

	val reviewSchema = StructType(Array(
		StructField ("reviewText", StringType, nullable=false),
		StructField ("overall",    DoubleType, nullable=false),
		StructField ("summary",    StringType, nullable=false)))

	// Read file and merge the text abd summary into a single text column

	def loadReviews (path: String): Dataset[ParsedReview] =
		spark
			.read
			.schema (reviewSchema)
			.json (path)
			.rdd
			.zipWithUniqueId
			.map[(Integer,String,Double)] { case (row,id) => (id.toInt, s"${row getString 2} ${row getString 0}", row getDouble 1) }
			.toDS
			.withColumnRenamed ("_1", "id" )
			.withColumnRenamed ("_2", "text")
			.withColumnRenamed ("_3", "overall")
			.as[ParsedReview]

  	// Load the GLoVe embeddings file

  	def loadGlove (path: String): Dataset[Embedding] =
		spark
			.read
			.text (path)
			.map  { _ getString 0 split " " }
			.map  (r => (r.head, r.tail.toList.map (_.toDouble))) // yuck!
			.withColumnRenamed ("_1", "word" )
			.withColumnRenamed ("_2", "vec")
			.as[Embedding]

	def parse(reviews: Dataset[ParsedReview]): Dataset[FormattedData] = {

		val tokenizer = new Tokenizer().setInputCol("text").setOutputCol("words")
		val wordsData = tokenizer.transform(reviews)

		wordsData.as[FormattedData]
	}

	def flatten(formattedData: Dataset[FormattedData]): Dataset[WordData] = {
		formattedData.flatMap {
			d => d._4.map {
				w => (d._1, d._2, d._3, w)
			}
		}
			.withColumnRenamed("_1","id")
			.withColumnRenamed("_2","text")
			.withColumnRenamed("_3","overall")
			.withColumnRenamed("_4","word")
			.as[WordData]
	}

	def join(gloveData: Dataset[Embedding], flatData: Dataset[WordData]): Dataset[PreVector] = {
		flatData
			.join(gloveData, "word")
			.select("id","vec")
			.groupBy("id")
			.agg(collect_list("vec"))
			.withColumnRenamed("collect_list(vec)", "vec")
			.as[PreVector]
	}

	def avgVectors(joinedData: Dataset[PreVector]): Dataset[IdVectorList] = {
		joinedData
			.map {
				x => (x._1, Vectors.dense(x._2.transpose.map {
					x => (x.sum / x.size.toDouble)
				}.toArray)) 
			}
			.withColumnRenamed("_1", "id")
			.withColumnRenamed("_2", "features")
			.as[IdVectorList]
	}

	def joinPrep(formattedData: Dataset[FormattedData], avgVecData: Dataset[IdVectorList]): Dataset[VectorOverall] = {
		avgVecData
			.join(formattedData, "id")
			.select("features", "overall")
			.as[VectorOverall]
	}

	def adjustScale(joinOverall: Dataset[VectorOverall]): Dataset[VectorOverall] = {
		joinOverall.map {
				x => (x._1, x._2 match {
					case (1.0) => 0.0
					case (2.0) => 0.0
					case (3.0) => 1.0
					case (4.0) => 2.0
					case (5.0) => 2.0
				})
			}
			.withColumnRenamed("_1", "features")
			.withColumnRenamed("_2", "label")
			.as[VectorOverall]
	}

	def main(args: Array[String]) = {
		//("./data/glove_6B_100d.txt") // 100d
		//("./data/glove_6B_300d.txt") // 300d		
		val gloveData = loadGlove("./data/glove_6B_100d.txt")

		// "./data/reviews_Musical_Instruments_5.json" // Musical Instruments
		// "./data/Patio_Lawn_and_Garden_5.json" // Patio, lawn and garden
		// "./data/reviews_Sports_and_Outdoors_5.json" // Sports and Outdoors
		val reviews = loadReviews ("./data/reviews_Musical_Instruments_5.json") 

		val formattedData = parse(reviews)		
		
		val flatData = flatten(formattedData)
		
		val joinedData = join(gloveData, flatData)

		val avgVecData = avgVectors(joinedData)

		val joinOverall = joinPrep(formattedData, avgVecData)

		val perceptronData = adjustScale(joinOverall)

		val splits = perceptronData.randomSplit(Array(0.8, 0.2))
		val train = splits(0)
		val test = splits(1)

		//val layers = Array[Int](300, 250, 50, 3) // for 300d
		val layers = Array[Int](100, 60, 20, 3) // for 100d
		
		val trainer = new MultilayerPerceptronClassifier()
			.setLayers(layers)
  			.setBlockSize(128)
  			.setSeed(1234L)
  			.setMaxIter(15)
				
		// --- Using MultiClassificationEvaluator ---		

		//val model = trainer.fit(train)
		
		// --- Using k-fold crossvalidation.

		val paramGrid = new ParamGridBuilder().build()
		val evaluator = new MulticlassClassificationEvaluator()
			.setMetricName("f1")
		
		val crossVal = new CrossValidator()
			.setEstimator(trainer)
			.setEvaluator(evaluator)
			.setEstimatorParamMaps(paramGrid)
			.setNumFolds(10)
			
		val model = crossVal
			.fit(train)
	
		// compute accuracy on the test set
		val result = model.transform(test)
		val predictionAndLabels = result.select("prediction", "label")
		
		val accuracy = new MulticlassClassificationEvaluator()
			.setMetricName("accuracy").evaluate(predictionAndLabels)
		val recall = new MulticlassClassificationEvaluator()
			.setMetricName("weightedRecall").evaluate(predictionAndLabels)
		val precision = new MulticlassClassificationEvaluator()
			.setMetricName("weightedPrecision").evaluate(predictionAndLabels)
		val fMeasure = new MulticlassClassificationEvaluator()
			.setMetricName("f1").evaluate(predictionAndLabels)
		println(s"Accuracy: $accuracy\nRecall: $recall\nPrecision: $precision\nF-measure: $fMeasure")

		spark.stop
	}

}
