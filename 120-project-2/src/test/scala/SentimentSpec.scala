import org.scalatest.{FreeSpec, Matchers, BeforeAndAfterAll}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Dataset
import org.apache.spark.ml.feature.Tokenizer
import org.apache.spark.ml.linalg.Vectors

class SentimentSpec extends FreeSpec with Matchers with BeforeAndAfterAll {

  org.apache.log4j.Logger getLogger "org"  setLevel (org.apache.log4j.Level.WARN)
  org.apache.log4j.Logger getLogger "akka" setLevel (org.apache.log4j.Level.WARN)

  val spark =  SparkSession.builder
    .appName ("Sentiment")
    .master  ("local[12]")
    .getOrCreate

  override def afterAll = spark.stop

  import spark.implicits._
  import Main._

  val glove  = Main.loadGlove ("./data/glove_6B_100d.txt")

  def createParsedReview(review: Seq[(Int,String,Double)]): Dataset[ParsedReview] = {
	review
		.toDF("id","text","overall")
		.as[ParsedReview]
  }

  def pipeline(review: Dataset[ParsedReview]): Dataset[VectorOverall] = {
  	val formattedData = parse(review)		
	val flatData = flatten(formattedData)
	val joinedData = join(glove, flatData)
	val avgVecData = avgVectors(joinedData)
	val joinOverall = joinPrep(formattedData, avgVecData)
	adjustScale(joinOverall)
  }

  "Tests"   - {

    "parsing" - {
      "tokenize correctly" in { 
		val review = createParsedReview(Seq((0, "Excellent garden chair", 4.0), (1, "Not at all good", 2.0)))

		val flat = flatten(parse(review))
		val flatArr = flat.head(7)

		assert(flatArr(0)._4 == "excellent")
		assert(flatArr(1)._4 == "garden")
		assert(flatArr(2)._4 == "chair")
		assert(flatArr(3)._4 == "not")
		assert(flatArr(4)._4 == "at")
		assert(flatArr(5)._4 == "all")
		assert(flatArr(6)._4 == "good")
	}
    }
	
	"flatten" - {
		"flatten a three word sentence to three rows" in {
			val review = createParsedReview(Seq((0, "This is great", 4.0)))
			val flatData = flatten(parse(review))
			assert(flatData.count() == 3)
		}
	}

	"adjust scale" - {
		"adjusting score 1.0 to 0.0" in {
			val review = createParsedReview(Seq(
				(0, "awful", 1.0)
			))
			val perceptronData = pipeline(review)
			assert(perceptronData.first()._2 == 0.0)
		}
		"adjusting score 2.0 to 0.0" in {
			val review = createParsedReview(Seq(
				(0, "ok", 2.0)
			))
			val perceptronData = pipeline(review)
			assert(perceptronData.first()._2 == 0.0)
		}
		"adjusting score 3.0 to 1.0" in {
			val review = createParsedReview(Seq(
				(0, "neutral", 3.0)
			))
			val perceptronData = pipeline(review)
		}
		"adjusting score 4.0 to 2.0" in {
			val review = createParsedReview(Seq(
				(0, "good", 4.0)
			))
			val perceptronData = pipeline(review)
			assert(perceptronData.first()._2 == 2.0)
		}
		"adjusting score 5.0 to 2.0" in {
			val review = createParsedReview(Seq(
				(0, "great", 5.0)
			))
			val perceptronData = pipeline(review)
			assert(perceptronData.first()._2 == 2.0)
		}
	}
	
	"averageing vectors" - {
		"average of three vectors correct" in {
			val prevector = Seq(
					(0, List(List(1.0, 2.0, 4.0), List(2.0,3.0,26.0), List(3.0,7.0,6.0)))
			).toDS()
				.withColumnRenamed("_1", "id")
				.withColumnRenamed("_2", "vec")
				.as[PreVector]

			val avg = avgVectors(prevector)
			val vector = avg.first()._2

			assert(vector == Vectors.dense(2.0,4.0,12.0))
		}
	}
  }

}
