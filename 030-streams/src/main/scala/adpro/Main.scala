// Advanced Programming
// Andrzej Wąsowski, IT University of Copenhagen

package adpro
import Stream._


//See "Main" as a Java-like "main" method. 
object Main extends App {
    
    println("Welcome to Streams, the ADPRO 030 class!!")

    val l1 :Stream[Int] = empty

    val l2 :Stream[Int]= cons(1, cons(2, cons (3, empty)))

    println (l1.headOption)
    println (l2.headOption)
    println(l2)

    //Exercise 2 test
    println(l1.toList)
    println(l2.toList)

    //Exercise 3 test
    //The test case terminates with no exceptions as a Stream (produced
    //by 'naturals' is lazy, meaning that it only evaluate the functions 
    //on the needed elements.
    println(naturals.take(1000000000).drop(41).take(10).toList)

    //Exercise 4 test
    //The test case terminates with no exceptions as a Stream (produced
    //by 'naturals' is lazy, meaning that it only evaluate the functions 
    //on the needed elements.
    println(naturals.takeWhile(_<1000000000).drop(100).take(50).toList)

    //Exercise 5 test
    println(naturals.forAll (_ < 0))
    //The test case will crash, as it tries to check all natural numbers.
    //println(naturals.forAll (_ >=0))

    //forAll and exists is fine to use on finite streams, as it is sure to
    //terminate at a finite time.

    //Exercise 7
    println(naturals.takeWhile2(_<1000000000).drop(100).take(50).toList)

    //Exercise 8.1
    //println(naturals.map (_*2).drop (30).take (50).toList)

    //Exercise 8.2
    //println(naturals.drop(42).filter (_%2 ==0).take (30).toList)

    //Exercise 8.3
    //println(naturals.append (naturals))
    //println(naturals.take(10).append(naturals).take(20).toList)

    //Exercise 8.4
    //println(naturals.flatMap (to _).take (100).toList)
    //println(naturals.flatMap (x =>from (x)).take (100).toList)

    //Exercise 9
    println(l1.fibs.take(7).toList)

    //Exercise 10
    //println(l1.from2(10).take(7).toList)
    //println(l1.fib2.take(7).toList)
    //println(l2.take2(3).toList)
}
