// Advanced Programming
// Andrzej Wąsowski, IT University of Copenhagen
//
// meant to be compiled, for example: fsc Stream.scala
// Group number: 3
//
// AUTHOR1: Martin Edvardsen (medv@itu.dk)
// TIME1: 5,5 hours <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR2: Jannik Halberg Homann (jhom@itu.dk)
// TIME2: 5,5 hours <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)

package adpro

sealed trait Stream[+A] {
  import Stream._

  def headOption () :Option[A] =
    this match {
      case Empty => None
      case Cons(h,t) => Some(h())
    }

  def tail :Stream[A] = this match {
      case Empty => Empty
      case Cons(h,t) => t()
  }

  def foldRight[B] (z : =>B) (f :(A, =>B) => B) :B = this match {
      case Empty => z
      case Cons (h,t) => f (h(), t().foldRight (z) (f))
      // Note 1. f can return without forcing the tail
      // Note 2. this is not tail recursive (stack-safe) It uses a lot of stack
      // if f requires to go deeply into the stream. So folds sometimes may be
      // less useful than in the strict case
    }

  // Note 1. eager; cannot be used to work with infinite streams. So foldRight
  // is more useful with streams (somewhat opposite to strict lists)
  def foldLeft[B] (z : =>B) (f :(A, =>B) =>B) :B = this match {
      case Empty => z
      case Cons (h,t) => t().foldLeft (f (h(),z)) (f)
      // Note 2. even if f does not force z, foldLeft will continue to recurse
    }

  def exists (p : A => Boolean) :Boolean = this match {
      case Empty => false
      case Cons (h,t) => p(h()) || t().exists (p)
      // Note 1. lazy; tail is never forced if satisfying element found this is
      // because || is non-strict
      // Note 2. this is also tail recursive (because of the special semantics
      // of ||)
    }

  //Exercise 2
  def toList: List[A] =
    this match {
      case Empty => List.empty[A]
      case Cons(h,t) => h()::t().toList
    }

  //Exercise 3
  def take(n: Int): Stream[A] =
    this match {
      case Empty => Empty
      case Cons(h,t) => if (n==1) Stream(h()) else cons(h(), t().take(n-1))
    }

  def drop(n: Int): Stream[A] =
    this match {
      case Empty => Empty
      case Cons(h,t) => if (n==0) cons(h(), t()) else t().drop(n-1)
    }
  
  //TODO: Answer the question from the exercise description

  //Exercise 4
  def takeWhile(p: A => Boolean): Stream[A] =
    this match {
      case Empty => Empty
      case Cons(h,t) => if (!p(h())) Stream(h()) else cons(h(), t().takeWhile(p))
    }


  //Exercise 5
  def forAll(p: A => Boolean): Boolean =
    this match {
      case Empty => true
      case Cons(h,t) => if (!p(h())) false else t().forAll(p)
    }

  //Exercise 6
  def takeWhile2(p: A => Boolean): Stream[A] =
    foldRight[Stream[A]](empty)((h, t) => if (!p(h)) empty else cons(h, t))

  //Exercise 7
  def headOption2 () :Option[A] =
    foldRight[Option[A]] (None) ((h, _) => Some(h))

  //Exercise 8 The types of these functions are omitted as they are a part of the exercises
  def map[B] (f: A => B): Stream[B] =
    foldRight[Stream[B]] (empty) ((h, t) => cons(f(h), t))

  def filter (f: A => Boolean): Stream[A] =
    foldRight[Stream[A]] (empty) ((h, t) => if(f(h)) cons(h, t) else t)

  def append[B >: A] (that: => Stream[B]): Stream[B] =
    foldRight[Stream[B]] (that) ((h, t) => cons(h, t))
   
  def flatMap[B] (f: A => Stream[B]): Stream[B] =
    foldRight (Stream.empty[B]) ((h, t) => f(h).append(t))

  //Exercise 09
  //Put your answer here: The implementation of find will only apply filter()
  //                      and headOption to the returned and preceding elements
  //                      in a Stream, whereas they will be applied to all elements
  //                      in a list.

  //TODO: Answer this.

  //Exercise 10
  //Put your answer here: l1.fibs.take(7).toList to produce the requested lazy stream.
  //                      This would be possible without a stream, if fib was a val.
  def fibs: Stream[Int] = {
    def inner (a: => Int, b: => Int): Stream[Int] = {
      Cons(() => a, () => inner(b, a + b))
    }
    inner (0, 1)
  }

  //Exercise 11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] =
    f(z) match {
      case None => empty
      case Some((a, s)) => cons(a, unfold(s)(f))
    }


  //Exercise 12
  def fib2: Stream[Int]  =
    unfold(0, 1) { case (a, b) => Some(a, (b, a+b)) }

  def from2 (n: Int): Stream[Int] =
    unfold (n) (n => Some((n, n + 1)))

  //Exercise 13
  def map2[B] (f: A => B): Stream[B] =
    unfold(this) {
        case Empty        => None
        case Cons(h, t) => Some(f(h()), t())
    }

  def take2 (n: Int): Stream[A] =
    unfold((this, n)) {
      case (Empty, n) => None
      case (Cons(h, t), 1)            => Some((h(), (Stream.empty[A], 0)))
      case (Cons(h, t), n) if (n > 1) => Some((h(), (t(), n - 1)))
    }

  def takeWhile3 (p: A => Boolean): Stream[A] = 
    unfold(this) {
        case Empty => None
        case Cons(h, t) if (p(h())) => Some(h(), t())
        case Cons(h, t)             => Some(h(), Stream.empty[A])
    }

  def zipWith2[B,C] (s: Stream[B]) (f: (A, B) => C): Stream[C] =
    unfold((this, s)) {
        case (Cons(h, t), Cons(h1, t1)) => Some(f(h(), h1()), (t(), t1()))
        case _                          => None
    }
}


case object Empty extends Stream[Nothing]
case class Cons[+A](h: ()=>A, t: ()=>Stream[A]) extends Stream[A]

object Stream {

  def empty[A]: Stream[A] = Empty

  def cons[A] (hd: => A, tl: => Stream[A]) :Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def apply[A] (as: A*) :Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))
    // Note 1: ":_*" tells Scala to treat a list as multiple params
    // Note 2: pattern matching with :: does not seem to work with Seq, so we
    //         use a generic function API of Seq

  def unfold2[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = {
    f(z) match {
      case None => Stream.empty[A]
      case Some((a,s)) => cons(a, unfold2(s)(f))
    }
  }

  //Exercise 1
  def from(n:Int):Stream[Int]=cons(n,from(n+1))

  def to(n:Int):Stream[Int]= cons(0,from(n-1))

  //def fib2mth(): Stream[Int] = unfold((0,1)){ case ((x,y)) => Some((x, (y,x+y)))
  //}

  val fib2: Stream[Int] = unfold2((0,1)){ case ((x,y)) => Some((x, (y,x+y))) }//fib2mth()

  val naturals: Stream[Int] = from(0)

  val fibs: Stream[Int] = {
    def inner(a: => Int, b: => Int): Stream[Int] =
      cons(a, inner(b, a+b))
    inner(0,1)
  }

  def from2(n: Int): Stream[Int] = {
    unfold2(n)(x => Some(x, (x+1)))
  }
}
