// Andrzej Wąsowski, Advanced Programming

package adpro.parsing

// I used Scala's standard library lists, and scalacheck Props in this set,
// instead of those developed by the book.

import java.util.regex._
import scala.util.matching.Regex

// we need this for higher kinded polymorphism
import language.higherKinds
// we need this for introducing internal DSL syntax
import language.implicitConversions

trait Parsers[ParseError, Parser[+_]] { self =>

  def run[A] (p: Parser[A]) (input: String): Either[ParseError,A]
  implicit def char (c: Char): Parser[Char]
  implicit def double (d: Double): Parser[Double] // added
  implicit def string (s: String): Parser[String]
  implicit def operators[A] (p: Parser[A]): ParserOps[A] = ParserOps[A](p)
  def or[A] (s1: Parser[A], s2: =>Parser[A]): Parser[A]
  def listOfN[A] (n: Int, p: Parser[A]): Parser[List[A]]

  def map[A,B] (p: Parser[A]) (f: A => B): Parser[B]
  def many[A](p: Parser[A]): Parser[List[A]]
  def succeed[A](a: A): Parser[A] = string("") map (_ => a)
  def slice[A](p: Parser[A]): Parser[String]
  def product[A,B](p: Parser[A], p2: =>Parser[B]): Parser[(A,B)]
  def flatMap[A,B](p: Parser[A])(f: A => Parser[B]): Parser[B]

  implicit def regex(r: Regex): Parser[String]
  implicit def asStringParser[A] (a: A) (implicit f: A => Parser[String]): ParserOps[String] = ParserOps(f(a))

  case class ParserOps[A](p: Parser[A]) {

    def |[B>:A] (p2: Parser[B]): Parser[B] = self.or (p,p2)
    def or[B>:A] (p2: Parser[B]): Parser[B] = self.or (p,p2)
    def **[B] (p2: Parser[B]): Parser[(A,B)] = self.product (p,p2)
    def product[B] (p2: Parser[B]): Parser[(A,B)] = self.product (p,p2)

    def map[B] (f: A => B): Parser[B] = self.map (p) (f)
    def flatMap[B] (f: A => Parser[B]): Parser[B] = self.flatMap (p) (f)
    def many: Parser[List[A]] = self.many[A] (p)
    def slice: Parser[String] = self.slice (p)
  }

  object Laws {

    // Storing the laws in the trait -- the will be instantiated when we have
    // concrete implementation.  Still without a concrete implementation they
    // can be type checked, when we compile.  This tells us that the
    // construction of the laws is type-correct (the first step for them
    // passing).

    import org.scalacheck._
    import org.scalacheck.Prop._

    val runChar = Prop.forAll { (c: Char) => run(char(c))(c.toString) == Right(c) }
    val runString = Prop.forAll { (s: String) => run(string(s))(s) == Right(s) }

    val listOfN1 = Prop.protect (run(listOfN(3, "ab" | "cad"))("ababcad") == Right("ababcad"))
    val listOfN2 = Prop.protect (run(listOfN(3, "ab" | "cad"))("cadabab") == Right("cadabab"))
    val listOfN3 = Prop.protect (run(listOfN(3, "ab" | "cad"))("ababab") == Right("ababab"))
    val listOfN4 = Prop.protect (run(listOfN(3, "ab" | "cad"))("ababcad") == Right("ababcad"))
    val listOfN5 = Prop.protect (run(listOfN(3, "ab" | "cad"))("cadabab") == Right("cadabab"))
    val listOfN6 = Prop.protect (run(listOfN(3, "ab" | "cad"))("ababab") == Right("ababab"))

    def succeed[A] (a: A) = Prop.forAll { (s: String) => run(self.succeed(a))(s) == Right(a) }

    // Not planning to run this (would need equality on parsers), but can write
    // for typechecking:

    def mapStructurePreserving[A] (p: Parser[A]): Boolean =
      map(p)(a => a) == p
  }


  // Exercise 1

  def manyA[A]: Parser[Int] = 
    char('a').many.map(n => n.size)

  // Exercise 2

  def map2[A,B,C] (p: Parser[A], p2: Parser[B]) (f: (A,B) => C): Parser[C] =
    //p.flatMap(a => p2.flatMap(b => succeed(f(a,b))))
    (p ** p2).map(f.tupled)
    //(p ** p2).flatMap(a => succeed(f(a._1, a._2)))

  def many1[A] (p: Parser[A]): Parser[List[A]] = 
    map2 (p, many(p)) (_ :: _)

  // Exercise 3

  def digitTimesA[A]: Parser[Int] = {
    val dig: Regex = "[0-9]".r   
    dig.flatMap(s => listOfN(s.toInt, 'a')).map(n => n.size)
  }

  // Exercise 4

  def product_[A,B] (p: Parser[A], p2: Parser[B]): Parser[(A,B)] =
    p.flatMap(a => p2.flatMap(b => succeed((a, b))))

  def map2_[A,B,C] (p: Parser[A], p2: Parser[B]) (f: (A,B) => C): Parser[C] =
    p.flatMap(a => p2.flatMap(b => succeed(f(a,b)))) 

  // Exercise 5

  def map_[A,B] (p: Parser[A]) (f: A => B): Parser[B] =
    p.flatMap(a => succeed(f(a)))

}

trait JSON

object JSON {
  case object JNull extends JSON
  case class JNumber(get: Double) extends JSON
  case class JString(get: String) extends JSON
  case class JBool(get: Boolean) extends JSON
  case class JArray(get: IndexedSeq[JSON]) extends JSON
  case class JObject(get: Map[String, JSON]) extends JSON

  // The first argument is the parsers implementation P (that we don't have).
  // We write this code with only having the interface

  //TA COMMENTS:
  //You do not handle whitespaces.

  def jsonParser[ParseErr,Parser[+_]] (P: Parsers[ParseErr,Parser]): Parser[JSON] = {

    import P._

    // primitives
    // ignored
    val quote: Parser[Char] = char('"')
    val comma: Parser[Char] = char(',')
    val colon: Parser[Char] = char(':')
    val lSqBracket: Parser[Char] = char('[')
    val rSqBracket: Parser[Char] = char(']')
    val lCurlyBracket: Parser[Char] = char('{')
    val rCurlyBracket: Parser[Char] = char('}')
    
    // needed
    val spaces: Parser[String] = char(' ').many.slice
    
    // booleans
    def boolParser: Parser[JBool] = string("true").or(string("false")).map(a => a match {
      case "true" => JBool(true)
      case "false" => JBool(false)
    })
    
    // doubles
    def dblParser: Parser[JNumber] = {
      val dblReg: Regex = "[+\\-]?(?:0|[1-9]\\d*)(?:\\.\\d*)?(?:[eE][+\\-]?\\d+)?".r
      regex(dblReg).map(d => JNumber(d.toDouble))  
    }

    // strings
    def unquotedStrParser: Parser[JString] = {
      val strReg: Regex = "[^\"]+".r  
      val strParser: Parser[JString] = regex(strReg).map(JString(_))
      (quote ** strParser ** quote).map({ case ((_, b), _) => b })
    }

    // literals
    val literalParser: Parser[JSON] = {
      boolParser | dblParser | unquotedStrParser
    }

    //TA COMMENTS:
    //Your elemParser is incorrect, as it either matches a 
    //single "v", or a "v,". When you then wrap that in many, 
    //you will effectively get the regex (v|v,)*, which means 
    //that you can match [vvvvvv] or [v,vvv,vv,v,] and such.

    //Instead you should do a product of v and (,v)*, 
    //and then handle the empty case separately.

    // arrays
    val elemParser: Parser[JSON] = {
      literalParser.or(literalParser ** comma ** spaces).map({ case (a: JSON, _, _) => a })
    }

    val arrParser: Parser[JArray] = {
      (lSqBracket ** spaces ** many(elemParser) ** spaces ** rSqBracket).map({
        case ((((_, _), c), _), _) => JArray(c.toIndexedSeq)
      })
    }

    // literals + arrays
    val valParser: Parser[JSON] = {
      boolParser | dblParser | unquotedStrParser | arrParser
    }

    // keys
    val keyParser: Parser[String] = {
      unquotedStrParser.map(a => a.get)
    }

    // object
    // TA COMMENTS:
    //In your object, you have wrapped the keyParser and valParser 
    //in many separately, meaning you will match 
    //{ k1k2k3k4k5 : v1v2v3v4v4 }. To fix this, simply wrap the whole 
    //thing in many (That is: many (keyParser ** spaces ** colon ** 
    //spaces ** many(valParser))). Note that this has the same problem 
    //regarding trailing commas as for arrays.

    (lCurlyBracket ** spaces ** many(keyParser ** spaces ** colon ** spaces ** many(valParser) ** spaces ** rCurlyBracket).map({
      case ((((((((_, _), c), _), _), _), g), _), _) => JObject((c.zip(g).toMap))
    })
  }
}
