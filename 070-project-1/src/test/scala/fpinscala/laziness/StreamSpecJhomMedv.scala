// Advanced Programming
// Andrzej Wasowski, IT University of Copenhagen

package fpinscala.laziness
import scala.language.higherKinds

import org.scalatest.FlatSpec
import org.scalatest.prop.Checkers
import org.scalacheck.Gen.posNum
import org.scalacheck._
import org.scalacheck.Prop._
import Arbitrary.arbitrary

// If you comment out all the import lines below, then you test the Scala
// Standard Library implementation of Streams. Interestingly, the standard
// library streams are stricter than those from the book, so some laziness tests
// fail on them :)

import stream00._    // uncomment to test the book solution
// import stream01._ // uncomment to test the broken headOption implementation
// import stream02._ // uncomment to test another version that breaks headOption

class StreamSpecJhomMedv extends FlatSpec with Checkers {

  import Stream._
  
  // GENERATORS
  // An example generator of random finite non-empty streams
  def list2stream[A] (la :List[A]): Stream[A] = 
    la.foldRight (empty[A]) (cons[A] (_,_))

  // In ScalaTest we use the check method to switch to ScalaCheck's internal DSL
  def genNonEmptyStream[A] (implicit arbA :Arbitrary[A]) :Gen[Stream[A]] =
    for { la <- arbitrary[List[A]] suchThat (_.nonEmpty)}
    yield list2stream (la)

  // the implict makes the generator available in the context
  implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])

  //For headOption:
    //- it should return None on an empty stream;  (already included in
    //  the examples)
    //- it should return some for a non-empty stream;  (already included
    //  in the examples)
    //- headOption should not force the tail of the stream.
  behavior of "headOption"

  // a scenario test:

  it should "return None on an empty Stream (01)" in {
    assert(empty.headOption == None)
  }

  // a property test:
  it should "return the head of the stream packaged in Some (02)" in check {
    ("singleton" |:
      Prop.forAll { (n :Int) => cons (n,empty).headOption == Some (n) } ) &&
    ("random" |:
      Prop.forAll { (s :Stream[Int]) => s.headOption != None } )

  }

  it should "return exception if the tail is forced (03)" in check {
    ("tail" |:
      Prop.forAll { (n :Int) => cons (n, 
        throw new RuntimeException("Tail was forced")).headOption == Some (n) })
  }

  behavior of "take"
  //For take:
  //- take should not force any heads nor any tails of the Stream it
  //  manipulates
  it should "return exception if the head or tail is forced (04)" in check {
      ("tail" |:
        Prop.forAll { (n :Int) => cons (n, 
          throw new RuntimeException("Tail was forced")).take(n)
          true;})
      ("head" |:
        Prop.forAll { (s :Stream[Int], n :Int) => 
          cons (throw new RuntimeException("Tail was forced"), s).take(n)
          true;})
  }
  //- take(n) does not force (n+1)st head ever (even if we force all
  //  elements of take(n))
  it should "return true if head is not forced even if we force tail (05)" in 
  {
    lazy val strm: Stream[Int] = 
      cons(1, cons(2, 
        cons(throw new RuntimeException("2nd head forced"), empty)))
    assert(strm.take(2).toList == List(1, 2))
  }
  //- s.take(n).take(n) == s.take(n) for any Stream s and any n
  //  (idempotency)
  it should "return true if take upholds idempotency (06)" in check {
    Prop.forAll { (s :Stream[Int], n :Int) => 
      s.take(n).take(n).toList == s.take(n).toList }
  }
  //- empty.take(n) == empty for any n
  it should "return true if empty.take(n) == empty (07)" in 
  {
    Prop.forAll { (n :Int) => 
      empty.take(n) == empty }
  }

  behavior of "drop"
  //For drop:
  //- s.drop(n).drop(m) == s.drop(n+m) for any n, m (additivity)
  it should "return true if drop upholds additivity (08)" in check {
    implicit def posInt = Arbitrary[Int] (Gen.posNum[Int])
    Prop.forAll { (n: Int, m: Int, s: Stream[Int]) => 
      s.drop(n).drop(m).toList == s.drop(n+m).toList }
  }
  //- s.drop(n) does not force any of the dropped elements heads
  it should "return exception if the head is forced (09)" in check {
    Prop.forAll { (s :Stream[Int], n :Int) => 
      cons (throw new RuntimeException("head was forced"), s).drop(n) 
      true;}
  }
  //- s.drop(-5) == s
  it should "return true if s.drop(-5) == s (10)" in 
  {
    lazy val strm :Stream[Int] = Stream(1, 2, 3)
    assert(strm.drop(-5).toList == strm.toList)
  }

  //- s.drop(0) == s
  it should "return true if s.drop(0) == s (11)" in 
  {
    lazy val strm :Stream[Int] = Stream(1, 2, 3)
    assert(strm.drop(0).toList == strm.toList)
  }

  //- the above should hold even if we force some stuff in the tail
  it should "return true if head is not forced even if we force tail (12)" in 
  {
    lazy val strm: Stream[Int] = 
      cons(1, cons(throw new RuntimeException("2nd head forced"), 
        cons(42, empty)))
    assert(strm.drop(2).headOption == Some(42))
  }
  
  behavior of "map"
  //For map:
  //- x.map(id) == x (where id is the identity function)
  it should "return true if identity function does not change s (13)" in check {
    Prop.forAll { (s :Stream[Int]) => s.map(identity).toList == s.toList }
  }
  //- map terminates on infinite streams
  it should "terminate on inifinite streams (14)" in {
    assert(from(0).map(identity) != null)
  }
  //Map works correctly
  it should "return true if map works correctly (15)" in 
  {
    lazy val strm :Stream[Int] = Stream(1, 2, 3)
    assert(strm.map(_ + 1).toList == List(2, 3, 4))
  }

  //For append:
  //- append should append properly (s.take(10).append(s.drop(10)).toList == 
  //  s.toList)
  it should "return true if the stream was appended correctly (16)" in check {
    Prop.forAll { (s :Stream[Int]) => 
      s.take(10).append(s.drop(10)).toList == s.toList }
  }
  //- append should not force any heads nor any tails of the Stream it
  //  appends
  it should "return exception if the head or tail is forced (17)" in check {
    ("tail" |:
      Prop.forAll { (s :Stream[Int], n :Int) => s.append(cons (n, 
        throw new RuntimeException("Tail was forced")))
        true;})
    ("head" |:
      Prop.forAll { (s :Stream[Int], n :Int) => s.append(
        cons (throw new RuntimeException("Tail was forced"), s))
        true;})
  }
}
